package config

import (
	"bei-idx-portal-api-v2/constants"
	"log"
	"os"

	"github.com/spf13/viper"
)

type Config struct {
	Server ServerConfig
	DB     DBConfig
}

type ServerConfig struct {
	Addr string
}

type DBConfig struct {
	Name            string
	Host            string
	MaxOpenConn     int
	MaxIdleConn     int
	ConnMaxLifetime int
}

func InitConfig() Config {
	viper.SetConfigName(".env")
	if os.Getenv("ENV") == constants.ENV_STAGING {
		viper.SetConfigName(".env-" + constants.ENV_STAGING)
	}

	if os.Getenv("ENV") == constants.ENV_PRODUCTION {
		viper.SetConfigName(".env-" + constants.ENV_PRODUCTION)
	}

	viper.AddConfigPath(".")

	var configuration Config

	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("Error reading config file, %s", err)
	}
	err := viper.Unmarshal(&configuration)
	if err != nil {
		log.Fatalf("unable to decode into struct, %v", err)
	}

	return configuration
}
