package utils

import (
	"bei-idx-portal-api-v2/constants"
	"bytes"
	"crypto/rand"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"math"
	randMath "math/rand"
	"regexp"
	"strconv"
	"strings"
	"time"
)

const (
	idCode = "+62"
)

var (
	phoneNumberRegex = regexp.MustCompile("^[+]?[0-9]{8,}$")
	emailRegex       = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
)

// ValidateEmail return true if email address has a valid format
func ValidateEmail(email string) bool {
	return emailRegex.MatchString(email)
}

//func HashPassword(password string) (string, error) {
//	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
//	return string(bytes), err
//}
//
//func CheckPasswordHash(password, hash string) bool {
//	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
//	return err == nil
//}

// NormalizePhoneNumber standarized mobile phone number format
func NormalizePhoneNumber(number string) string {
	number = strings.Replace(strings.TrimSpace(number), " ", "", -1)

	if len(number) < 8 {
		return number
	}

	if strings.HasPrefix(number, "0") {
		return fmt.Sprintf("%s%s", idCode, number[1:])
	}

	if strings.HasPrefix(number, "+62") {
		return fmt.Sprintf("%s%s", idCode, number[3:])
	}

	origNumber := number
	for {
		if len(number) <= 2 || !strings.HasPrefix(number, "62") {
			break
		}

		number = number[2:]
	}

	if len(number) < 8 {
		number = origNumber
	}

	return fmt.Sprintf("%s%s", idCode, number)
}

// ValidatePhoneNumber return true if number has a valid phone number format
func ValidatePhoneNumber(number string) bool {
	number = NormalizePhoneNumber(number)
	return phoneNumberRegex.MatchString(number)
}

func trimQuote(s string) string {
	if len(s) > 0 && s[0] == '"' {
		s = s[1:]
	}
	if len(s) > 0 && s[len(s)-1] == '"' {
		s = s[:len(s)-1]
	}
	return s
}

// DateString return string representation of a date pointer
func DateString(dateTime *time.Time) string {
	if dateTime == nil {
		return ""
	}

	return dateTime.Format(constants.DateFormatStd)
}

func DateTimeString(dateTime *time.Time) string {
	if dateTime == nil {
		return ""
	}

	return dateTime.Format(constants.DateFromStd)
}
func DateTimeZoneString(dateTimeZone *time.Time) string {
	if dateTimeZone == nil {
		return ""
	}
	return dateTimeZone.Format(time.RFC3339)
}

func StructToByte(data interface{}) []byte {
	reqBodyBytes := new(bytes.Buffer)
	json.NewEncoder(reqBodyBytes).Encode(data)

	return reqBodyBytes.Bytes()
}

func PrintStruct(data interface{}) {
	dataByte := StructToByte(data)
	fmt.Printf(string(dataByte))
	return
}

func NullStringScan(value *string) string {
	if value == nil {
		return ""
	}
	return *value
}

func SeparateFullname(fullname string) (firstName, middleName, lastName string, err error) {
	names := strings.Split(fullname, " ")
	switch len(names) {
	case 0:
		err = errors.New("Insert Name")
	case 1:
		firstName = names[0]
		lastName = names[0]
	case 2:
		firstName = names[0]
		lastName = names[1]
	default:
		firstName = names[0]
		middleName = strings.Join(names[1:len(names)-1], " ")
		lastName = names[len(names)-1]
	}

	return firstName, middleName, lastName, err
}

func FormatMediaPath(rootPath string, value *string) string {
	if NullStringScan(value) != "" {
		return fmt.Sprintf("%s%s", rootPath, NullStringScan(value))
	} else {
		return ""
	}
}

func RandomOtp(max int) int {
	var table = [...]byte{'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'}
	b := make([]byte, max)
	n, err := io.ReadAtLeast(rand.Reader, b, max)
	if n != max {
		panic(err)
	}
	for i := 0; i < len(b); i++ {
		b[i] = table[int(b[i])%len(table)]
	}
	randomNumber, _ := strconv.Atoi(string(b))
	return randomNumber
}

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func RandomString(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[randMath.Intn(len(letterBytes))]
	}
	return string(b)
}

func CreateNestedSlice(slice []string, n int) [][]string {
	results := [][]string{}
	if len(slice) < n {
		results = [][]string{
			slice,
		}
		return results
	}

	maxIndex := int(math.Ceil(float64(len(slice)) / float64(n)))
	for i := 0; i < maxIndex; i++ {
		subResult := []string{}

		startIndex := i * n
		endIndex := i*n + n
		if endIndex > len(slice) {
			subResult = append(subResult, slice[startIndex:]...)
		} else {
			subResult = append(subResult, slice[startIndex:endIndex]...)
		}

		results = append(results, subResult)
	}

	return results
}
