package utils

import (
	"bei-idx-portal-api-v2/constants"
	"errors"
	"fmt"
	"strings"
)

func ErrDuplicate(m string) error {
	errs := strings.Split(m, constants.PGDuplicateConstraint)
	s := trimQuote(fmt.Sprintf("%s", errs[1]))
	return errors.New(fmt.Sprintf("Duplicate for data %s already in database", s))
}

func ErrDataNotFound(m string) error {
	errs := fmt.Sprintf(constants.ErrDataNotFound, m)

	return errors.New(errs)
}
