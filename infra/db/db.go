package db

import (
	"bei-idx-portal-api-v2/config"
	"database/sql"
	"errors"
	"log"
	"sync"
	"sync/atomic"
	"time"

	"github.com/jmoiron/sqlx"
)

type DB struct {
	mtx    sync.RWMutex
	driver string
	dbs    []*sql.DB
	count  uint64
}

func scatter(n int, fn func(idx int) error) error {
	errCh := make(chan error, n)
	for i := 0; i < n; i++ {
		go func(idx int) {
			errCh <- fn(idx)
		}(i)
	}

	var err error
	for i := 0; i < cap(errCh); i++ {
		if e := <-errCh; e != nil {
			err = e
		}
	}

	return err
}

// Open concurrently opens each underlying physical db
func Open(dbSetting *config.DBConfig) (*DB, error) {
	if dbSetting == nil {
		return nil, errors.New("database setting is required")
	}

	if dbSetting.Name == "" {
		return nil, errors.New("database driver name should not empty")
	}

	dsns := []string{dbSetting.Host}

	db := &DB{
		driver: dbSetting.Name,
		dbs:    make([]*sql.DB, len(dsns)),
	}

	err := scatter(len(db.dbs), func(idx int) error {
		dbConn, err := sql.Open(dbSetting.Name, dsns[idx])
		if err != nil {
			return err
		}

		dbConn.SetMaxOpenConns(dbSetting.MaxOpenConn)
		dbConn.SetMaxIdleConns(dbSetting.MaxIdleConn)
		dbConn.SetConnMaxLifetime(time.Duration(dbSetting.ConnMaxLifetime) * time.Second)
		db.dbs[idx] = dbConn

		return nil
	})

	if err != nil {
		return nil, err
	}

	log.Printf("success connect to database %s", dbSetting.Host)

	return db, nil
}

// spread all slave connection to all available slaves
func (db *DB) slave(n int) int {
	if n <= 1 {
		return 0
	}

	return int(1 + (atomic.AddUint64(&db.count, 1) % uint64(n-1)))
}

// Slave returns one of the physical databases which is a slave
func (db *DB) Slave() *sqlx.DB {
	db.mtx.RLock()
	defer db.mtx.RUnlock()

	return sqlx.NewDb(db.dbs[db.slave(len(db.dbs))], db.driver)
}

// Master returns the master physical database
func (db *DB) Master() *sqlx.DB {
	db.mtx.RLock()
	defer db.mtx.RUnlock()

	return sqlx.NewDb(db.dbs[0], db.driver)
}
