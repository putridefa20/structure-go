package constants

import "errors"

const (
	PGDuplicateConstraint = "pq: duplicate key value violates unique constraint "
	ErrDataNotFound       = "No Data: %s"
	ErrIsNotPrivateEvent  = "Event with id %s is not private event"
)

var (
	ErrMissingLoginValues   = errors.New("missing Username or Password")
	ErrFailedAuthentication = errors.New("incorrect Username or Password")
	ErrExpiredToken         = errors.New("token is expired")
	ErrExistEmail           = errors.New("Email Already Registered")
	ErrEmptyAuthHeader      = errors.New("auth header is empty")
)

var (
	// ErrDataNotFound           = errors.New("No Data")
	ErrBeginTransaction                    = errors.New("Failed To Begin Transaction")
	ErrGoogleIdAppleIdIsUsed               = errors.New("GoogleId or AppleId already used")
	ErrInvalidEmailAddressOfPrimaryAccount = errors.New("Invalid Email Address of Primary Account")
	ErrInvalidOrExpiredOtpNumber           = errors.New("Invalid or Expired OTP Number")
	ErrBindingAccount                      = errors.New("Account binding can't be done, try another primary account")
	ErrUnknownBindingFlag                  = errors.New("Unknown Binding Flag")
)

//required error messages
var (
	ErrPhoneNumberIsRequired = errors.New("Phone Number is  Required")
	ErrNameIsRequired        = errors.New("Name is required")
	ErrDescriptionIsRequired = errors.New("Description is required")
	ErrIdIsRequired          = errors.New("Id is required")
	ErrRegionIdIsRequired    = errors.New("region_id is required")
	ErrChannelIdIsRequired   = errors.New("channel_id is required")
)
