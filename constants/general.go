package constants

const (
	AppName                   = ""
	Authorization             = "Authorization"
	DateFormatStd             = "2006-01-02"
	TimeFormatStd             = "15:04:05"
	TimeFormatShort           = "15:04"
	DateFromStd               = "2006-01-02 15:04:05"
	DefaultPassword           = "difotPaswod"
	HttpRequestFormFileKey    = "file"
	HttpRequestFormFolderName = "type"
)
