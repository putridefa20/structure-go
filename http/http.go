package http

import (
	"bei-idx-portal-api-v2/context/repository"
	"bei-idx-portal-api-v2/context/service"
	"bei-idx-portal-api-v2/infra/db"
)

func initRepoCtx(db *db.DB) *repository.RepoCtx {
	//
	return &repository.RepoCtx{
		//
	}
}

func initServiceCtx(ctx *repository.RepoCtx) *service.ServiceCtx {
	//
	return &service.ServiceCtx{
		//
	}
}

func HTTP() error {

	// initial config
	// ctx := context.Background()

	// cfg := config.InitConfig()

	// this Pings the database trying to connect, panics on error
	// use sqlx.Open() for sql.Open() semantics

	// db, err := db.Open(&cfg.DB)
	// if err != nil {
	// 	log.Fatalln(err)
	// }

	// init repo ctx
	// repoCtx := initRepoCtx(db)

	// init service ctx
	// serviceCtx := initServiceCtx(repoCtx)

	// districtsHandler := districtsHandler.NewDistrictsHandler(serviceCtx)
	// r := routers.InitialRouter(
	// 	districtsHandler,
	// )
	// fmt.Println("Starting server on the addr :8080 ")
	// log.Fatal(http.ListenAndServe(":8080", r))
	return nil
}
