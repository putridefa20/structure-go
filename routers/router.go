package routers

import "github.com/gorilla/mux"

const (
	GET   = "GET"
	POS   = "POST"
	PUT   = "PUT"
	PATCH = "PATCH"
	DEL   = "DELETE"
)

func InitialRouter(
// districtsHandler districts.DistrictsHandlerInterface,
) *mux.Router {
	r := mux.NewRouter()

	//districts
	// r.HandleFunc("/district/create", districtsHandler.CreateDistricts).Methods(POS)
	// r.HandleFunc("/district/get", districtsHandler.GetDistrictsList).Methods(GET)
	// r.HandleFunc("/districts/update", districtsHandler.UpdateDistricts).Methods(PUT)
	// r.HandleFunc("/districts/delete/{districts_id}", districtsHandler.DeleteDistricts).Methods(DEL)
	// r.HandleFunc("/districts/get/{districts_id}", districtsHandler.GetDistrictsById).Methods(GET)
	return r
}
